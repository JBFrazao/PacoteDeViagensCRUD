﻿namespace PacoteDeViagensCRUD
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormNovoPacote : Form
    {
        private ListaDePacotes Pacotes = new ListaDePacotes();

        private FormPrincipal formularioPrincipal;

        public FormNovoPacote(FormPrincipal formularioPrincipal, ListaDePacotes Pacotes)
        {
            InitializeComponent();
            this.formularioPrincipal = formularioPrincipal;
            this.Pacotes = Pacotes;
            TxtBoxIDPacote.Text = Pacotes.Index.ToString();
        }

        private void FormNovoPacote_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtBoxDescricao.Text))
            {
                MessageBox.Show("Preencha a descrição!");
                return;
            }
            if (NumBoxPreco.Value == 0)
            {
                MessageBox.Show("Insira um preço!");
                return;
            }

            Pacotes.CriarPacote(Pacotes.ObterIndex() , TxtBoxDescricao.Text, Convert.ToDouble(NumBoxPreco.Value));
            Close();
            formularioPrincipal.Show();
            MessageBox.Show("Pacote criado com sucesso!");
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            formularioPrincipal.Show();
            Close();
        }

        private void BtnFechar_MouseHover(object sender, EventArgs e)
        {
            BtnFechar.Cursor = Cursors.Hand;

        }


    }
}
