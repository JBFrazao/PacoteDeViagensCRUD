﻿namespace PacoteDeViagensCRUD
{
    using Modelos;
    using System;
    using System.Windows.Forms;
    using System.Drawing;
    using System.Collections.Generic;

    public partial class FormPrincipal : Form
    {

        private ListaDePacotes Pacotes = new ListaDePacotes();

        private List<PacoteDeViagem> ListaDePacotes;

        private FormNovoPacote formularioNovoPacote;

        private FormApagarPacote formularioApagarPacote;

        private FormEditarPacote formularioEditarPacote;

        private FormPesquisarPacote formularioPesquisarPacote;



        public FormPrincipal()
        {
            InitializeComponent();
            ListaDePacotes = Pacotes.CarregarPacotes();
        }


        #region Buttons
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void BtnNovo_Click(object sender, EventArgs e)
        {
            formularioNovoPacote = new FormNovoPacote(this, Pacotes);
            formularioNovoPacote.Show();
            Hide();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            formularioEditarPacote = new FormEditarPacote(this, Pacotes);
            formularioEditarPacote.Show();
            Hide();


        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            formularioPesquisarPacote = new FormPesquisarPacote(this, Pacotes);
            formularioPesquisarPacote.Show();
            Hide();
        }

        private void BtnApagar_Click(object sender, EventArgs e)
        {
            formularioApagarPacote = new FormApagarPacote(this, Pacotes);
            formularioApagarPacote.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit(); 
        }
        #endregion


        #region Tooltip
        private void BtnFechar_MouseHover(object sender, EventArgs e)
        {
            BtnFechar.Cursor = Cursors.Hand;
        }

        private void BtnNovo_MouseHover(object sender, EventArgs e)
        {
            BtnNovo.Cursor = Cursors.Hand;
        }

        private void BtnPesquisar_MouseHover(object sender, EventArgs e)
        {
            BtnPesquisar.Cursor = Cursors.Hand;

        }

        private void BtnEditar_MouseHover(object sender, EventArgs e)
        {
            BtnEditar.Cursor = Cursors.Hand;

        }

        private void BtnApagar_MouseHover(object sender, EventArgs e)
        {
            BtnApagar.Cursor = Cursors.Hand;

        }
        #endregion


        #region Menu inferior
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("EasyTravel - v1.1.0", "Sobre...");
        }

        private void guardarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Pacotes.GuardarFicheiro();
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pacotes.AbrirFicheiro();
            ListaDePacotes = null;
            ListaDePacotes = Pacotes.Output;
        }

        private void guardarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pacotes.GuardarComo();
        }

        private void TSMFicheiro_DropDownOpened(object sender, EventArgs e)
        {
            TSMFicheiro.ForeColor = Color.Black;
        }

        private void TSMFicheiro_DropDownClosed(object sender, EventArgs e)
        {
            TSMFicheiro.ForeColor = Color.White;

        }
        #endregion
    }
}
