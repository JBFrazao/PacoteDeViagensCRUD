﻿namespace PacoteDeViagensCRUD
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbBoxApagar = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnFechar = new System.Windows.Forms.Button();
            this.BtnApagar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // CmbBoxApagar
            // 
            this.CmbBoxApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmbBoxApagar.FormattingEnabled = true;
            this.CmbBoxApagar.Location = new System.Drawing.Point(25, 80);
            this.CmbBoxApagar.Name = "CmbBoxApagar";
            this.CmbBoxApagar.Size = new System.Drawing.Size(550, 21);
            this.CmbBoxApagar.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu Condensed", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 55);
            this.label4.TabIndex = 10;
            this.label4.Text = "Apagar";
            // 
            // BtnFechar
            // 
            this.BtnFechar.BackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.BGArtboard_1;
            this.BtnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnFechar.FlatAppearance.BorderSize = 0;
            this.BtnFechar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFechar.Location = new System.Drawing.Point(558, 12);
            this.BtnFechar.Name = "BtnFechar";
            this.BtnFechar.Size = new System.Drawing.Size(30, 30);
            this.BtnFechar.TabIndex = 2;
            this.BtnFechar.UseVisualStyleBackColor = false;
            this.BtnFechar.Click += new System.EventHandler(this.BtnCancelar_Click);
            this.BtnFechar.MouseHover += new System.EventHandler(this.BtnFechar_MouseHover);
            // 
            // BtnApagar
            // 
            this.BtnApagar.BackColor = System.Drawing.Color.Transparent;
            this.BtnApagar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_7;
            this.BtnApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnApagar.FlatAppearance.BorderSize = 0;
            this.BtnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnApagar.Location = new System.Drawing.Point(270, 122);
            this.BtnApagar.Name = "BtnApagar";
            this.BtnApagar.Size = new System.Drawing.Size(60, 60);
            this.BtnApagar.TabIndex = 1;
            this.BtnApagar.UseVisualStyleBackColor = false;
            this.BtnApagar.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(-6, 71);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(611, 44);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(600, 190);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BtnFechar);
            this.Controls.Add(this.BtnApagar);
            this.Controls.Add(this.CmbBoxApagar);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormApagarPacote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Apagar Pacote";
            this.Load += new System.EventHandler(this.FormApagarPacote_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbBoxApagar;
        private System.Windows.Forms.Button BtnApagar;
        private System.Windows.Forms.Button BtnFechar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}