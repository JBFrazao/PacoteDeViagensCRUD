﻿namespace PacoteDeViagensCRUD
{
    using PacoteDeViagensCRUD.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarPacote : Form
    {
        private ListaDePacotes Pacotes = new ListaDePacotes();

        private FormPrincipal formularioPrincipal;

        public FormApagarPacote(FormPrincipal formularioPrincipal, ListaDePacotes Pacotes)
        {
            InitializeComponent();
            this.formularioPrincipal = formularioPrincipal;
            this.Pacotes = Pacotes;
            CmbBoxApagar.DataSource = Pacotes.Output;
        }

        private void FormApagarPacote_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CmbBoxApagar.SelectedIndex > -1)
            {
                DialogResult myResult;
                myResult = MessageBox.Show("Tem a certeza que quer apagar este Pacote?", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (myResult == DialogResult.Yes)
                {
                    Pacotes.ApagarPacote(CmbBoxApagar.SelectedIndex);
                    CmbBoxApagar.DataSource = null;
                    CmbBoxApagar.DataSource = Pacotes.Output;
                }
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            formularioPrincipal.Show();
            Close();
        }

        private void BtnFechar_MouseHover(object sender, EventArgs e)
        {
            BtnFechar.Cursor = Cursors.Hand;
        }
    }
}
