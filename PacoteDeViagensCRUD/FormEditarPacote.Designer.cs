﻿namespace PacoteDeViagensCRUD
{
    partial class FormEditarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBoxIDPacote = new System.Windows.Forms.TextBox();
            this.TxtBoxDescricao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnLeft = new System.Windows.Forms.Button();
            this.BtnRight = new System.Windows.Forms.Button();
            this.BtnGravar = new System.Windows.Forms.Button();
            this.BtnFechar = new System.Windows.Forms.Button();
            this.NumBoxPreco = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtBoxIDPacote
            // 
            this.TxtBoxIDPacote.BackColor = System.Drawing.Color.White;
            this.TxtBoxIDPacote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtBoxIDPacote.ForeColor = System.Drawing.Color.Black;
            this.TxtBoxIDPacote.Location = new System.Drawing.Point(92, 87);
            this.TxtBoxIDPacote.Multiline = true;
            this.TxtBoxIDPacote.Name = "TxtBoxIDPacote";
            this.TxtBoxIDPacote.ReadOnly = true;
            this.TxtBoxIDPacote.Size = new System.Drawing.Size(380, 20);
            this.TxtBoxIDPacote.TabIndex = 0;
            // 
            // TxtBoxDescricao
            // 
            this.TxtBoxDescricao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TxtBoxDescricao.BackColor = System.Drawing.Color.White;
            this.TxtBoxDescricao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtBoxDescricao.ForeColor = System.Drawing.Color.Black;
            this.TxtBoxDescricao.Location = new System.Drawing.Point(92, 113);
            this.TxtBoxDescricao.Multiline = true;
            this.TxtBoxDescricao.Name = "TxtBoxDescricao";
            this.TxtBoxDescricao.Size = new System.Drawing.Size(380, 20);
            this.TxtBoxDescricao.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(18, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID Pacote :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(18, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Descrição :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(38, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Preço :";
            // 
            // BtnLeft
            // 
            this.BtnLeft.BackColor = System.Drawing.Color.Transparent;
            this.BtnLeft.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_5;
            this.BtnLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnLeft.FlatAppearance.BorderSize = 0;
            this.BtnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLeft.Location = new System.Drawing.Point(22, 197);
            this.BtnLeft.Name = "BtnLeft";
            this.BtnLeft.Size = new System.Drawing.Size(30, 30);
            this.BtnLeft.TabIndex = 6;
            this.BtnLeft.Text = " ";
            this.BtnLeft.UseVisualStyleBackColor = false;
            this.BtnLeft.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnRight
            // 
            this.BtnRight.BackColor = System.Drawing.Color.Transparent;
            this.BtnRight.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_6;
            this.BtnRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnRight.FlatAppearance.BorderSize = 0;
            this.BtnRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRight.Location = new System.Drawing.Point(83, 197);
            this.BtnRight.Name = "BtnRight";
            this.BtnRight.Size = new System.Drawing.Size(30, 30);
            this.BtnRight.TabIndex = 7;
            this.BtnRight.UseVisualStyleBackColor = false;
            this.BtnRight.Click += new System.EventHandler(this.BtnRight_Click);
            // 
            // BtnGravar
            // 
            this.BtnGravar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGravar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_4;
            this.BtnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnGravar.FlatAppearance.BorderSize = 0;
            this.BtnGravar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGravar.Location = new System.Drawing.Point(428, 182);
            this.BtnGravar.Name = "BtnGravar";
            this.BtnGravar.Size = new System.Drawing.Size(60, 60);
            this.BtnGravar.TabIndex = 8;
            this.BtnGravar.UseVisualStyleBackColor = false;
            this.BtnGravar.Click += new System.EventHandler(this.BtnGravar_Click);
            // 
            // BtnFechar
            // 
            this.BtnFechar.BackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.BGArtboard_1;
            this.BtnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnFechar.FlatAppearance.BorderSize = 0;
            this.BtnFechar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFechar.Location = new System.Drawing.Point(458, 12);
            this.BtnFechar.Name = "BtnFechar";
            this.BtnFechar.Size = new System.Drawing.Size(30, 30);
            this.BtnFechar.TabIndex = 9;
            this.BtnFechar.UseVisualStyleBackColor = false;
            this.BtnFechar.Click += new System.EventHandler(this.BtnCancelar_Click);
            this.BtnFechar.MouseHover += new System.EventHandler(this.BtnFechar_MouseHover);
            // 
            // NumBoxPreco
            // 
            this.NumBoxPreco.BackColor = System.Drawing.Color.White;
            this.NumBoxPreco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NumBoxPreco.DecimalPlaces = 2;
            this.NumBoxPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumBoxPreco.ForeColor = System.Drawing.Color.Black;
            this.NumBoxPreco.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NumBoxPreco.Location = new System.Drawing.Point(92, 136);
            this.NumBoxPreco.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumBoxPreco.Name = "NumBoxPreco";
            this.NumBoxPreco.Size = new System.Drawing.Size(112, 34);
            this.NumBoxPreco.TabIndex = 10;
            this.NumBoxPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumBoxPreco.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu Condensed", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 55);
            this.label4.TabIndex = 11;
            this.label4.Text = "Editar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(210, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 31);
            this.label5.TabIndex = 12;
            this.label5.Text = "€";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(-8, 67);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(512, 109);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // FormEditarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(500, 250);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NumBoxPreco);
            this.Controls.Add(this.BtnFechar);
            this.Controls.Add(this.BtnGravar);
            this.Controls.Add(this.BtnRight);
            this.Controls.Add(this.BtnLeft);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtBoxDescricao);
            this.Controls.Add(this.TxtBoxIDPacote);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditarPacote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editar Pacote";
            this.Load += new System.EventHandler(this.FormEditarPacote_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtBoxIDPacote;
        private System.Windows.Forms.TextBox TxtBoxDescricao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnLeft;
        private System.Windows.Forms.Button BtnRight;
        private System.Windows.Forms.Button BtnGravar;
        private System.Windows.Forms.Button BtnFechar;
        private System.Windows.Forms.NumericUpDown NumBoxPreco;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}