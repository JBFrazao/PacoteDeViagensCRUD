﻿namespace PacoteDeViagensCRUD
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnNovo = new System.Windows.Forms.Button();
            this.BtnEditar = new System.Windows.Forms.Button();
            this.BtnPesquisar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TSMFicheiro = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMSobre = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnFechar = new System.Windows.Forms.Button();
            this.BtnApagar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnNovo
            // 
            this.BtnNovo.BackColor = System.Drawing.Color.White;
            this.BtnNovo.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1;
            this.BtnNovo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnNovo.FlatAppearance.BorderSize = 0;
            this.BtnNovo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnNovo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnNovo.Font = new System.Drawing.Font("Ubuntu Light", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.BtnNovo.ForeColor = System.Drawing.Color.Gray;
            this.BtnNovo.Location = new System.Drawing.Point(14, 123);
            this.BtnNovo.Name = "BtnNovo";
            this.BtnNovo.Size = new System.Drawing.Size(90, 90);
            this.BtnNovo.TabIndex = 0;
            this.BtnNovo.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnNovo.UseVisualStyleBackColor = false;
            this.BtnNovo.Click += new System.EventHandler(this.BtnNovo_Click);
            this.BtnNovo.MouseHover += new System.EventHandler(this.BtnNovo_MouseHover);
            // 
            // BtnEditar
            // 
            this.BtnEditar.BackColor = System.Drawing.Color.White;
            this.BtnEditar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_3;
            this.BtnEditar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnEditar.FlatAppearance.BorderSize = 0;
            this.BtnEditar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEditar.Font = new System.Drawing.Font("Ubuntu Light", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.BtnEditar.ForeColor = System.Drawing.Color.Gray;
            this.BtnEditar.Location = new System.Drawing.Point(206, 123);
            this.BtnEditar.Name = "BtnEditar";
            this.BtnEditar.Size = new System.Drawing.Size(90, 90);
            this.BtnEditar.TabIndex = 2;
            this.BtnEditar.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnEditar.UseVisualStyleBackColor = false;
            this.BtnEditar.Click += new System.EventHandler(this.BtnEditar_Click);
            this.BtnEditar.MouseHover += new System.EventHandler(this.BtnEditar_MouseHover);
            // 
            // BtnPesquisar
            // 
            this.BtnPesquisar.BackColor = System.Drawing.Color.White;
            this.BtnPesquisar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy;
            this.BtnPesquisar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnPesquisar.FlatAppearance.BorderSize = 0;
            this.BtnPesquisar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPesquisar.Font = new System.Drawing.Font("Ubuntu Light", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.BtnPesquisar.ForeColor = System.Drawing.Color.Gray;
            this.BtnPesquisar.Location = new System.Drawing.Point(110, 123);
            this.BtnPesquisar.Name = "BtnPesquisar";
            this.BtnPesquisar.Size = new System.Drawing.Size(90, 90);
            this.BtnPesquisar.TabIndex = 3;
            this.BtnPesquisar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnPesquisar.UseVisualStyleBackColor = false;
            this.BtnPesquisar.Click += new System.EventHandler(this.BtnPesquisar_Click);
            this.BtnPesquisar.MouseHover += new System.EventHandler(this.BtnPesquisar_MouseHover);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu Condensed", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(202, 55);
            this.label4.TabIndex = 11;
            this.label4.Text = "EasyTravel";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMFicheiro,
            this.TSMSobre});
            this.menuStrip1.Location = new System.Drawing.Point(9, 260);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(127, 24);
            this.menuStrip1.TabIndex = 13;
            // 
            // TSMFicheiro
            // 
            this.TSMFicheiro.BackColor = System.Drawing.Color.Transparent;
            this.TSMFicheiro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TSMFicheiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guardarToolStripMenuItem,
            this.guardarToolStripMenuItem1,
            this.guardarComoToolStripMenuItem});
            this.TSMFicheiro.ForeColor = System.Drawing.Color.White;
            this.TSMFicheiro.Name = "TSMFicheiro";
            this.TSMFicheiro.Size = new System.Drawing.Size(61, 20);
            this.TSMFicheiro.Text = "Ficheiro";
            this.TSMFicheiro.DropDownClosed += new System.EventHandler(this.TSMFicheiro_DropDownClosed);
            this.TSMFicheiro.DropDownOpened += new System.EventHandler(this.TSMFicheiro_DropDownOpened);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.BackColor = System.Drawing.Color.Transparent;
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.guardarToolStripMenuItem.Text = "Abrir";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.guardarToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem1
            // 
            this.guardarToolStripMenuItem1.Name = "guardarToolStripMenuItem1";
            this.guardarToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.guardarToolStripMenuItem1.Text = "Guardar";
            this.guardarToolStripMenuItem1.Click += new System.EventHandler(this.guardarToolStripMenuItem1_Click);
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.guardarComoToolStripMenuItem.Text = "Guardar como...";
            this.guardarComoToolStripMenuItem.Click += new System.EventHandler(this.guardarComoToolStripMenuItem_Click);
            // 
            // TSMSobre
            // 
            this.TSMSobre.ForeColor = System.Drawing.Color.White;
            this.TSMSobre.Name = "TSMSobre";
            this.TSMSobre.Size = new System.Drawing.Size(58, 20);
            this.TSMSobre.Text = "Sobre...";
            this.TSMSobre.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // BtnFechar
            // 
            this.BtnFechar.BackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.BGArtboard_1;
            this.BtnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnFechar.FlatAppearance.BorderSize = 0;
            this.BtnFechar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFechar.Font = new System.Drawing.Font("Ubuntu Light", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.BtnFechar.ForeColor = System.Drawing.Color.Gray;
            this.BtnFechar.Location = new System.Drawing.Point(362, 12);
            this.BtnFechar.Name = "BtnFechar";
            this.BtnFechar.Size = new System.Drawing.Size(30, 30);
            this.BtnFechar.TabIndex = 4;
            this.BtnFechar.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnFechar.UseVisualStyleBackColor = false;
            this.BtnFechar.Click += new System.EventHandler(this.button1_Click);
            this.BtnFechar.MouseHover += new System.EventHandler(this.BtnFechar_MouseHover);
            // 
            // BtnApagar
            // 
            this.BtnApagar.BackColor = System.Drawing.Color.White;
            this.BtnApagar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_2;
            this.BtnApagar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnApagar.FlatAppearance.BorderSize = 0;
            this.BtnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.WhiteSmoke;
            this.BtnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnApagar.Font = new System.Drawing.Font("Ubuntu Light", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.BtnApagar.Location = new System.Drawing.Point(302, 123);
            this.BtnApagar.Name = "BtnApagar";
            this.BtnApagar.Size = new System.Drawing.Size(90, 90);
            this.BtnApagar.TabIndex = 1;
            this.BtnApagar.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BtnApagar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.BtnApagar.UseVisualStyleBackColor = false;
            this.BtnApagar.Click += new System.EventHandler(this.BtnApagar_Click);
            this.BtnApagar.MouseHover += new System.EventHandler(this.BtnApagar_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(-10, 81);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(426, 176);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(404, 285);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BtnFechar);
            this.Controls.Add(this.BtnApagar);
            this.Controls.Add(this.BtnNovo);
            this.Controls.Add(this.BtnPesquisar);
            this.Controls.Add(this.BtnEditar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestor de Pacotes";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnNovo;
        private System.Windows.Forms.Button BtnApagar;
        private System.Windows.Forms.Button BtnEditar;
        private System.Windows.Forms.Button BtnPesquisar;
        private System.Windows.Forms.Button BtnFechar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TSMFicheiro;
        private System.Windows.Forms.ToolStripMenuItem TSMSobre;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
    }
}

