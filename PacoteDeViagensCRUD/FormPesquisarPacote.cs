﻿namespace PacoteDeViagensCRUD
{
    using Modelos;
    using System;
    using System.Windows.Forms;

    public partial class FormPesquisarPacote : Form
    {
        private ListaDePacotes Pacotes = new ListaDePacotes();

        private FormPrincipal formularioPrincipal;


        public FormPesquisarPacote(FormPrincipal formularioPrincipal, ListaDePacotes Pacotes)
        {
            InitializeComponent();
            this.formularioPrincipal = formularioPrincipal;
            this.Pacotes = Pacotes;
            DGVPacotes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            DGVPacotes.ColumnHeadersVisible = false;
            DGVPacotes.RowHeadersVisible = false;
            ActualizarDados();
        }

        private void FormPesquisarPacote_Load(object sender, EventArgs e)
        {

        }

        private void ActualizarDados()
        {
            DGVPacotes.DataSource = null;
            DGVPacotes.DataSource = Pacotes.Output;
          
        }

        private void BtnSair_Click(object sender, EventArgs e)
        {
            formularioPrincipal.Show();
            Close();
        }

        private void BtnFechar_MouseHover(object sender, EventArgs e)
        {
            BtnFechar.Cursor = Cursors.Hand;
        }

       
    }
}
