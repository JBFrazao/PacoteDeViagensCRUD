﻿namespace PacoteDeViagensCRUD
{
    partial class FormNovoPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtBoxDescricao = new System.Windows.Forms.TextBox();
            this.TxtBoxIDPacote = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.BtnFechar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NumBoxPreco = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxPreco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtBoxDescricao
            // 
            this.TxtBoxDescricao.BackColor = System.Drawing.Color.White;
            this.TxtBoxDescricao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtBoxDescricao.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TxtBoxDescricao.Location = new System.Drawing.Point(92, 113);
            this.TxtBoxDescricao.Multiline = true;
            this.TxtBoxDescricao.Name = "TxtBoxDescricao";
            this.TxtBoxDescricao.Size = new System.Drawing.Size(247, 20);
            this.TxtBoxDescricao.TabIndex = 0;
            // 
            // TxtBoxIDPacote
            // 
            this.TxtBoxIDPacote.BackColor = System.Drawing.Color.White;
            this.TxtBoxIDPacote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtBoxIDPacote.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TxtBoxIDPacote.Location = new System.Drawing.Point(92, 87);
            this.TxtBoxIDPacote.Multiline = true;
            this.TxtBoxIDPacote.Name = "TxtBoxIDPacote";
            this.TxtBoxIDPacote.ReadOnly = true;
            this.TxtBoxIDPacote.Size = new System.Drawing.Size(247, 20);
            this.TxtBoxIDPacote.TabIndex = 2;
            this.TxtBoxIDPacote.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.IconsArtboard_1_copy_4;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(150, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 60);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BtnFechar
            // 
            this.BtnFechar.BackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.BackgroundImage = global::PacoteDeViagensCRUD.Properties.Resources.BGArtboard_1;
            this.BtnFechar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnFechar.FlatAppearance.BorderSize = 0;
            this.BtnFechar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BtnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFechar.Location = new System.Drawing.Point(318, 12);
            this.BtnFechar.Name = "BtnFechar";
            this.BtnFechar.Size = new System.Drawing.Size(30, 30);
            this.BtnFechar.TabIndex = 4;
            this.BtnFechar.UseVisualStyleBackColor = false;
            this.BtnFechar.Click += new System.EventHandler(this.button2_Click);
            this.BtnFechar.MouseHover += new System.EventHandler(this.BtnFechar_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(18, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID Pacote :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(18, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Descrição :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(38, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Preço :";
            // 
            // NumBoxPreco
            // 
            this.NumBoxPreco.BackColor = System.Drawing.Color.White;
            this.NumBoxPreco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NumBoxPreco.DecimalPlaces = 2;
            this.NumBoxPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumBoxPreco.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.NumBoxPreco.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NumBoxPreco.Location = new System.Drawing.Point(92, 139);
            this.NumBoxPreco.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.NumBoxPreco.Name = "NumBoxPreco";
            this.NumBoxPreco.Size = new System.Drawing.Size(247, 34);
            this.NumBoxPreco.TabIndex = 8;
            this.NumBoxPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu Condensed", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(239, 55);
            this.label4.TabIndex = 9;
            this.label4.Text = "Novo Pacote";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(-76, 73);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(512, 109);
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // FormNovoPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(360, 261);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NumBoxPreco);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnFechar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TxtBoxIDPacote);
            this.Controls.Add(this.TxtBoxDescricao);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormNovoPacote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Novo Pacote";
            this.Load += new System.EventHandler(this.FormNovoPacote_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxPreco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtBoxDescricao;
        private System.Windows.Forms.TextBox TxtBoxIDPacote;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BtnFechar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NumBoxPreco;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}