﻿namespace PacoteDeViagensCRUD.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;

    public class ListaDePacotes
    {
        public List<PacoteDeViagem> Output { get; set; }
        public int Index { get; set; }
        public string Path { get; set; }

        public List<PacoteDeViagem> CarregarPacotes()
        {
            List<PacoteDeViagem> Output = new List<PacoteDeViagem>();
            Path = Application.StartupPath.ToString() + @"\..\..\Resources\PacotesDeViagem.txt";
            StreamReader sr;
            if (File.Exists(Path))
            {
                sr = File.OpenText(Path);
                string linha = "";
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = new string[3];
                    campos = linha.Split(';');
                    var pacote = new PacoteDeViagem
                    {
                        IDPacote = Convert.ToInt32(campos[0]),
                        Descricao = campos[1],
                        Preco = Convert.ToDouble(campos[2])
                    };
                    Index = Convert.ToInt32(campos[0]) + 1;
                    Output.Add(pacote);
                }

                sr.Close();

            }
            else
            {
                Output.Add(new PacoteDeViagem { IDPacote = 1, Descricao = "Pacote Bahamas - Hotel de luxo à beira mar. 7 noites", Preco = 9.99 });
                Output.Add(new PacoteDeViagem { IDPacote = 2, Descricao = "Pacote México - Cartel de luxo à beira mar. 5 noites", Preco = 9.99 });
                Output.Add(new PacoteDeViagem { IDPacote = 3, Descricao = "Pacote Médio Oriente - Harém de luxo à beira mar. 3 noites", Preco = 9.99 });
                Output.Add(new PacoteDeViagem { IDPacote = 4, Descricao = "Pacote Angola - Barraca de luxo à beira mar. 3 noites", Preco = 9.99 });
                Output.Add(new PacoteDeViagem { IDPacote = 5, Descricao = "Pacote Coreia do Norte - Destino de luxo de sonhos. 7 noites", Preco = 9.99 });
                Output.Add(new PacoteDeViagem { IDPacote = 6, Descricao = "Pacote Brasil - Hotel de luxo á beira da favela. 5 noites", Preco = 9.99 });
                StreamWriter sw = new StreamWriter(Path, false);
                try
                {
                    if (!File.Exists(Path))
                    {
                        sw = File.CreateText(Path);
                    }
                    foreach (var pacote in Output)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.IDPacote, pacote.Descricao, pacote.Preco);
                        sw.WriteLine(linha);
                    }
                    sw.Close();
                }
                catch (Exception erro) //objecto e da classe Exception
                {
                    MessageBox.Show(erro.Message); //e.Message faz o display da excepção

                }
                sw.Close();

            }
            this.Output = Output;    //Esta é a alteração (ListaAlunos1) e depois no final digo que a ListaAlunos(Propriedade) é igual é ListaAlunos1
            return Output;
        }   //CARREGA UM FICHEIRO PRE-DEFINIDO. SE NÃO EXISTIR, GERA ESSE FICHEIRO (UTILIZADO APENAS DE INÍCIO)

        public int ObterIndex()
        {
            CarregarPacotes();
            Index = Output[Output.Count - 1].IDPacote + 1;
            return Index;
        }   //OBTEM O INDEX A PARTIR DO REGISTO COM O ID MAIS ALTO

        public void CriarPacote(int ID, string descricao, double preco)
        {
            Output.Add(new PacoteDeViagem(ID, descricao, preco));
            Index = Index + 1;
        }   //ADICIONA UM PACOTE

        public void ApagarPacote(int index)
        {
            Output.RemoveAt(index);
        }   //ELIMINA O PACOTE COM BASE NUM INDEX

        public void EditarPacote(int index, string descricao, double preco)
        {
            Output[index].Descricao = descricao;
            Output[index].Preco = preco;
        }   //EDITA O PACOTA COM BASE NUM INDEX

        public List<PacoteDeViagem> AbrirFicheiro()
        {
            OpenFileDialog Abrir = new OpenFileDialog();
            Abrir.Filter = "Ficheiro de texto|*.txt";
            Abrir.Title = "Abrir ficheiro";
            Abrir.ShowDialog();
            StreamReader sr;
            try
            {
                if (File.Exists(Abrir.FileName))
                {
                    Output.Clear();
                    sr = File.OpenText(Abrir.FileName);
                    string linha = "";
                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];
                        campos = linha.Split(';');
                        var pacote = new PacoteDeViagem
                        {
                            IDPacote = Convert.ToInt32(campos[0]),
                            Descricao = campos[1],
                            Preco = Convert.ToDouble(campos[2])
                        };
                        Index = Convert.ToInt32(campos[0]) + 1;
                        Output.Add(pacote);
                    }
                    sr.Close();
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);
            }
            Path = Abrir.FileName;
            return Output;
        }   //CARREGA OS DADOS DE UM FICHEIRO E ALTERA O "PATH"

        public void GuardarFicheiro()
        {
            StreamWriter sw = new StreamWriter(Path, false); //"false" - se nao existir ficheiro
            try
            {
                if (!File.Exists(Path))
                {
                    sw = File.CreateText(Path);
                }
                foreach (var pacote in Output)
                {
                    string linha = string.Format("{0};{1};{2}", pacote.IDPacote, pacote.Descricao, pacote.Preco);
                    sw.WriteLine(linha);
                }
                sw.Close();
            }
            catch (Exception erro) //objecto e da classe Exception
            {
                MessageBox.Show(erro.Message); //e.Message faz o display da excepção

            }

            sw.Close();
        }   //GUARDA O FICHEIRO UTILIZANDO O "PATH", FAZENDO OVERWRITE SE JÁ EXISTIR

        public void GuardarComo()
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog Guardar = new SaveFileDialog();
            Guardar.Filter = "Ficheiro de texto|*.txt";
            Guardar.Title = "Guardar como...";
            Guardar.InitialDirectory = Path;
            Guardar.RestoreDirectory = false;
            Guardar.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (Guardar.FileName != "")
            {
                StreamWriter sw = new StreamWriter(Guardar.FileName, false); //"false" - se nao existir ficheiro
                try
                {
                    if (!File.Exists(Guardar.FileName))
                    {
                        sw = File.CreateText(Guardar.FileName);

                    }
                    foreach (var pacote in Output)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.IDPacote, pacote.Descricao, pacote.Preco);
                        sw.WriteLine(linha);
                    }
                    sw.Close();
                }
                catch (Exception erro) //objecto e da classe Exception
                {
                    MessageBox.Show(erro.Message); //e.Message faz o display da excepção
                }
                Path = Guardar.FileName;
                sw.Close();
            }
        }   //CRIA UM NOVO FICHEIRO E REGISTA OS DADOS, ALTERA TAMBÉM A PROP "PATH"
    }
}

