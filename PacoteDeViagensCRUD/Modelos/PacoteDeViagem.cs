﻿namespace PacoteDeViagensCRUD.Modelos
{
    public class PacoteDeViagem
    {
        
        private int _idpacote;
        private string _descricao;
        private double _preco;

        public int IDPacote
        {
            get { return _idpacote; }
            set { _idpacote = value; }
        }
        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }
        public double Preco
        {
            get { return _preco; }
            set { _preco = value; }
        }

        public PacoteDeViagem() : this(0, "Por favor preencher o campo", 00.00) { }

        public PacoteDeViagem(int ID, string descricao, double preco)
        {
            IDPacote = ID;
            Descricao = descricao;
            Preco = preco;
        }
        public PacoteDeViagem(PacoteDeViagem pacote)
        {
            Descricao = pacote.Descricao;
            Preco = pacote.Preco;
        }
        public override string ToString()
        {
            return string.Format("  {0}  |   {1}     »»»»    {2}€", IDPacote, Descricao, Preco);
        }
        public override int GetHashCode()
        {
            return _idpacote;
        }
    }
}
