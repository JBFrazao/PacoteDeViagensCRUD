﻿namespace PacoteDeViagensCRUD
{
    using PacoteDeViagensCRUD.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormEditarPacote : Form
    {
        private ListaDePacotes Pacotes = new ListaDePacotes();

        private FormPrincipal formularioPrincipal;
        
        private int x = 0;

        public FormEditarPacote(FormPrincipal formularioPrincipal, ListaDePacotes Pacotes)
        {
            InitializeComponent();
            this.formularioPrincipal = formularioPrincipal;
            this.Pacotes = Pacotes;
            ActualizaDados();
        }

        private void FormEditarPacote_Load(object sender, EventArgs e)
        {
           

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (x > 0)
            {
                x--;
            }
            else
            {
                x = Pacotes.Output.Count - 1;
            }
            ActualizaDados();

        }

        private void BtnRight_Click(object sender, EventArgs e)
        {
            if (x < Pacotes.Output.Count - 1)
            {
                x++;
            }
            else
            {
                x = 0;
            }
            ActualizaDados();

        }

        private void ActualizaDados()
        {
            TxtBoxIDPacote.Text = Pacotes.Output[x].IDPacote.ToString();
            TxtBoxDescricao.Text = Pacotes.Output[x].Descricao.ToString();
            NumBoxPreco.Text = Pacotes.Output[x].Preco.ToString();
        }

        private void BtnGravar_Click(object sender, EventArgs e)
        {
            DialogResult myResult;
            myResult = MessageBox.Show("Tem a certeza que quer gravar as alterações do Pacote?", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (myResult == DialogResult.Yes)
            {
                Pacotes.EditarPacote(x, TxtBoxDescricao.Text, Convert.ToDouble(NumBoxPreco.Value));
                ActualizaDados();
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            formularioPrincipal.Show();
            Close();
        }

        private void BtnFechar_MouseHover(object sender, EventArgs e)
        {
            BtnFechar.Cursor = Cursors.Hand;
        }
    }
}
